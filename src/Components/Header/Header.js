import React from 'react';
import logo from '../../assets/logo.png';
import '../../App.css';

const Header = () => {
    return (
        <header className="headers">
            <div className="container">
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a href="#" className="logo">
                    <img src={logo} alt="MyFolio"/>
                </a>
                <nav className="main-nav">
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">Home</a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">Premium PSD files</a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">UI kits</a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">Services</a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">My works</a>
                    {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                    <a href="#">Contact me</a>
                </nav>
            </div>
        </header>
    );
};

export default Header;