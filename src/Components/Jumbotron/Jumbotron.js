import React from 'react';
import '../../App.css';

const Jumbotron = () => {
    return (
        <div className="main-block">
            <div className="container">
                <h1 className="uppercase bore">Incididunt Bore</h1>
                <p className="text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta commodi et aperiam
                    inventore natus. Inventore amet praesentium pariatur fugit sunt repellat, nostrum voluptate
                    molestiae, excepturi, at officia! Tempore totam consequatur optio ipsa, repudiandae nesciunt
                    praesentium quos repellat alias maxime sint doloremque.</p>
                {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                <a href="#" className="link-btn">Know More</a>
            </div>
        </div>
    );
};

export default Jumbotron;