import React from 'react';

const AboutBlock = () => {
    return (
        <div className="container">
            <div className="aboutme">
                <h2 className="uppercase">About me</h2>
                <p className="textaboutme">Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam harum earum
                    optio alias sint
                    omnis neque, aliquid magni veritatis delectus labore a commodi rerum tempora <b>impedit maiores sit
                        fuga animi
                        accusamus ipsum</b> voluptates? Sequi excepturi ab unde fugiat quaerat accusamus!</p>
            </div>
            <div className="boxes">
                <div className="box bg1">
                    <h3 className="h3box">Our Story</h3>
                    <p className="pbox">Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae vero qui dolorem
                        mollitia
                        sapiente fugit aliquid amet!</p>
                </div>
                <div className="box bg2">
                    <h3 className="h3box">Recomendations</h3>
                    <p className="pbox">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Excepturi fugit minus
                        repellendus
                        culpa placeat! Laborum repudiandae nostrum est!</p>
                </div>
                <div className="box bg3">
                    <h3 className="h3box">The Main Ivent</h3>
                    <p className="pbox">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, fugiat et.
                        Odit, optio!</p>
                </div>
                <div className="box bg4">
                    <h3 className="h3box">Our Clients</h3>
                    <p className="pbox">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolore quasi eaque fuga
                        repudiandae
                        molestiae magni unde fugiat aut reprehenderit aliquam.</p>
                </div>
            </div>
        </div>
    );
};

export default AboutBlock;