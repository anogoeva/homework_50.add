import React from 'react';
import '../../App.css'

const Footer = () => {
    return (
        <footer>
            <div className="container">
                <p>© MyFolio</p>
            </div>
        </footer>
    );
};
export default Footer;