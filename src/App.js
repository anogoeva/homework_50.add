import './App.css';
import Header from "./Components/Header/Header";
import Jumbotron from "./Components/Jumbotron/Jumbotron";
import AboutBlock from "./Components/About-block/About-block";
import Footer from "./Components/Footer/Footer";

function App() {
    return (
        <div className="App">
            <Header/>
            <Jumbotron/>
            <AboutBlock/>
            <Footer/>
        </div>
    );
}

export default App;